import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'

admin.initializeApp();

export const formatData = functions.database.ref('/DHT11/Humidity/{pushId}/')
    .onCreate(
        (snapshot, context) => {
            const original = snapshot.val();
            const pushId = context.params.pushId;
            console.log(`Detected new measure ${original} with pushId ${pushId}`);
            return admin.database().ref('/DHT11_timestamp').push({
                value: original,
                timestamp: admin.database.ServerValue.TIMESTAMP
            });
        });

export const formatDataTemp = functions.database.ref('/DHT11/Temperature/{pushId}/')
        .onCreate(
            (snapshot, context) => {
                const original = snapshot.val();
                const pushId = context.params.pushId;
                console.log(`Detected new measure ${original} with pushId ${pushId}`);
                return admin.database().ref('/DHT11_timestampTemperature').push({
                    value: original,
                    timestamp: admin.database.ServerValue.TIMESTAMP
                });
            });

export const formatDataLux = functions.database.ref('/Photoresistor/{pushId}/')
            .onCreate(
                (snapshot, context) => {
                    const original = snapshot.val();
                    const pushId = context.params.pushId;
                    console.log(`Detected new measure ${original} with pushId ${pushId}`);
                    return admin.database().ref('/Photoresistor_timestamp').push({
                        value: original,
                        timestamp: admin.database.ServerValue.TIMESTAMP
                    });
                });