#include <ESP8266WiFi.h>        // Include the Wi-Fi library
#include <DHT.h>
#include <FirebaseArduino.h>

#define FIREBASE_AUTH "Zojx6nlQZPsrHmJ9O51hojJnI25r3zEjpVhrctNJ"
#define FIREBASE_HOST "airq-e073b.firebaseio.com"


#define DHTTYPE DHT22
#define DHTPIN D1

DHT dht(DHTPIN, DHTTYPE);
float temperature;
float humidity;



const char* ssid     = "hackathon";         // The SSID (name) of the Wi-Fi network you want to connect to
const char* password = "att4hack";     // The password of the Wi-Fi network

void setup() {
  Serial.begin(115200);         // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println('\n');
  
  WiFi.begin(ssid, password);             // Connect to the network
  Serial.print("Connecting to ");
  Serial.print(ssid); Serial.println(" ...");

  int i = 0;
  while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }

  Serial.println('\n');
  Serial.println("Connection established!");  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer

  dht.begin();
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH); 
}

void loop() 
{
  temperature = dht.readTemperature();
  humidity = dht.readHumidity();
  if (isnan(humidity) || isnan(temperature)) {  // Check if any reads failed and exit early (to try again).
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  int sensorValue = analogRead(A0);
  float voltage = sensorValue * (3.3 / 1023.0);

  
  Serial.println(temperature);
  Serial.println(humidity);
  Serial.println(voltage);

  delay(4000);
  
  Firebase.pushFloat("/DHT11_2/Humidity", humidity);         //setup path and send readings
  Firebase.pushFloat("/DHT11_2/Temperature", temperature);        //setup path and send readings
  Firebase.pushFloat("/Photoresistor_2/Value", voltage);
  



  
}
