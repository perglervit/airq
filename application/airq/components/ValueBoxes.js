import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  Platform,
  Text,
  View,
} from 'react-native';

import { headingStyle, styles } from '../css/styles';

export default function ValueBoxes(props) {

  let intesityOfSun = props.sunIntensity;

    return (
        <View style={styles.wrapperOfWrapperBox}>
          <View elevation={3} style={styles.wrapperBox}>
            <View style={styles.boxes}>

              <Text style={styles.roomName}>{props.roomName}</Text>

              <View style={styles.iconsBoxes}>
                <View style={styles.iconWithText}>
                  <Icon style={styles.icon} name={Platform.OS === 'ios' ? 'ios-thermometer' : 'md-thermometer'} size={28} color="#F45937"/>
                  <Text style={styles.text}>Temperature:</Text>
                </View>
                <View>            
                  <Text style={styles.valueOfSensors}>{props.temperature} °C</Text>
                </View>
              </View>

              <View style={styles.iconsBoxes}>
                <View style={styles.iconWithText}>
                  <Icon style={styles.icon} name={Platform.OS === 'ios' ? 'ios-water' : 'md-water'} size={28} color="#F45937"/>
                  <Text style={styles.text}>Humidity:</Text>
                </View>
                <View>            
                  <Text style={styles.valueOfSensors}>{props.humidity}%</Text>
                </View>
              </View>

              <View style={styles.iconsBoxes}>
                <View style={styles.iconWithText}>
                  <Icon style={styles.icon} name={Platform.OS === 'ios' ? 'ios-cloud' : 'md-cloud'} size={28} color="#F45937"/>
                  <Text style={styles.text}>CO2:</Text>
                </View>
                <View>            
                  <Text style={styles.valueOfSensors}>{props.coAmount} g/km</Text>
                </View>
              </View>

              <View style={styles.iconsBoxes}>
                <View style={styles.iconWithText}>
                  <Icon style={styles.icon} name={Platform.OS === 'ios' ? 'ios-shuffle' : 'md-shuffle'} size={28} color="#F45937"/>
                  <Text style={styles.text}>Atm. Pressure:</Text>
                </View>
                <View>            
                  <Text style={styles.valueOfSensors}>{props.pressure} kPa</Text>
                </View>
              </View>

              <View style={styles.iconsBoxes}>
                <View style={styles.iconWithText}>
                  <Icon 
                    style={styles.icon} 
                    name={intesityOfSun >= 150 ? (Platform.OS === 'ios' ? 'ios-sunny' : 'md-sunny') : (Platform.OS === 'ios' ? 'ios-moon' : 'md-moon')} 
                    size={28}
                    color="#F45937"
                  />
                  <Text style={styles.text}>Sun intensity</Text>
                </View>
                <View>            
                  <Text style={styles.valueOfSensors}>{props.sunIntensity} lx</Text>
                </View>
              </View>

            </View>
          </View>
        </View>
    );
  }
  