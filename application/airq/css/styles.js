import { StyleSheet } from 'react-native';

const headingStyle = StyleSheet.create({
    heading: {
      marginTop: 70,
      marginLeft: 60,
      flexDirection: 'column'
    },
    h1: {
      fontSize: 38,
      color: '#F45937'
    },
    h2: {
      fontSize: 28,
      color: '#b8b8b8'
    },
    h3: {
      fontSize: 18,
      color: "#858585",
      marginTop: 20,
    },
    center:{
      textAlign: 'center'
    }
  });

  const floorPlan = StyleSheet.create({
    heading: {
      marginTop: 70,
      marginLeft: 60,
      flexDirection: 'column'
    },
    h1: {
      fontSize: 38,
      color: '#F45937'
    },
    h2: {
      marginTop: 10,
      fontSize: 26,
      color: '#b8b8b8'
    },
  });

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFF',
      paddingTop: 25,
    },
    heading: {
      marginTop: 50,
      marginLeft: 60,
      flexDirection: 'column'
    },
    h1: {
      fontSize: 60,
      color: '#242424'
    },
    h2: {
      fontSize: 34,
      color: '#b8b8b8'
    },
    wrapperOfWrapperBox:{
      alignItems: "center",
    },
    wrapperBox:{
      width: 320,
      backgroundColor: "#fff",
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
      marginTop: 10,
      marginBottom: 10,
    },
    boxes:{
      width: 220,
      flexDirection: 'column',
      alignContent: "flex-start",
      paddingLeft: 10,
      paddingRight: 10,
      marginTop: 20,
      marginBottom: 20,
    },
    roomName:{
      fontSize: 22,
      fontWeight: "500",
    },
    iconsBoxes:{
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      paddingBottom: 10,
      paddingTop: 10,
    },
    iconWithText:{
      flexDirection: "row",
      alignItems: "center",
    },
    icon:{
      marginRight: 10,
    },
    text:{
      fontSize: 14,
      color: "#242424",
    },
    valueOfSensors:{
      fontSize: 18
    }
  });

  const overlayFloorStyles = StyleSheet.create({
    wrapper:{
      margin: 0,
      padding: 0,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-around",
    },
    h1:{
      fontSize: 32,
      fontWeight: 300,
    },
  }); 

  export { headingStyle, styles, floorPlan, overlayFloorStyles }