import React from 'react';
import { Platform, StatusBar, StyleSheet, View, Text } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from "react-navigation";
import Icon from 'react-native-vector-icons/Ionicons';
import MainScreen from './screens/HomeScreen'
import FloorScreen from './screens/FloorPlan' 
import GraphScreen from './screens/GraphsScreen'
import FloorScreen2 from './screens/FloorPlan2' 

import { Ionicons } from '@expo/vector-icons';


const AppNavigator = createBottomTabNavigator ({
  HomeScreen: MainScreen,
  FloorScreen: FloorScreen,
  FloorScreen2: FloorScreen2,
  GraphScreen: GraphScreen
},{
  initialRouteName: 'HomeScreen', 
  order: ['HomeScreen', 'FloorScreen', 'FloorScreen2', 'GraphScreen'],
  tabBarOptions: {
    showLabel: false,
    labelStyle: {
      fontSize: 15
    },
    style: {
      backgroundColor: "#fff",
      borderTopRightRadius: 20,
      borderTopLeftRadius: 20,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 1.84,
      borderTopColor: "transparent",
      elevation: 2,
    },
  },
  defaultNavigationOptions: ({navigation})=> ({
    tabBarIcon: ({focused, horizontal, tintColor})=>{
      const { routeName } = navigation.state;
      if(routeName === 'HomeScreen'){
        return (<Icon color={focused ? '#000' : '#bababa'} focused={focused} name={Platform.OS === 'ios' ? `ios-apps` : 'md-apps'} size={30} />)
      }
      if(routeName === 'FloorScreen'){
        return (<Icon color={focused ? '#000' : '#bababa'} focused={focused} name={Platform.OS === 'ios' ? 'ios-map' : 'md-map'} size={30} />)
      }
      if(routeName === 'FloorScreen2'){
        return (<Icon color={focused ? '#000' : '#bababa'} focused={focused} name={Platform.OS === 'ios' ? 'ios-map' : 'md-map'} size={30} />)
      }
      if(routeName === 'GraphScreen'){
        return (<Icon color={focused ? '#000' : '#bababa'} focused={focused} name={Platform.OS === 'ios' ? 'ios-settings' : 'md-settings'} size={30} />)
      }
      
    },
    tabBarOnPress:({navigation, defaultHandler})=>{
      if(navigation.state.key === 'FloorSection'){
        navigation.navigate('UserScreen')
      }else {
        defaultHandler()
      }
    }
  })
}
)
const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}