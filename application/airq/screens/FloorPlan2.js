import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  Text,
  View,
  TouchableOpacity,
  Platform
} from 'react-native';
import { db } from '../firebase';
import { Overlay } from 'react-native-elements'
import { floorPlan, styles, overlayFloorStyles } from '../css/styles'

let humidityRef = db.ref('/DHT11_2/Humidity').limitToLast(1);
let temperatureRef = db.ref('/DHT11_2/Temperature').limitToLast(1);
let luxRef = db.ref('/Photoresistor_2/Value').limitToLast(1);

export default class FloorPlan2 extends Component {
  state = {
    humidity: [],
    temperature: [],
    lux: [],
    tempVisible: false,
    humVisible: false,
    luxVisible: false
  }

  componentDidMount(){
    humidityRef.on('value' , (snap) => {
      let data = snap.val();
      let humidity = Object.values(data);
      this.setState({ humidity })
    })
    temperatureRef.on('value' , (snap) => {
      let data = snap.val();
      let temperature = Object.values(data);
      this.setState({ temperature })
    })
    luxRef.on('value' , (snap) => {
      let data = snap.val();
      let lux = Object.values(data);
      lux = Math.round(250 * lux)
      this.setState({ lux })
    })
  }

  dht = () => {
    this.setState({ tempVisible: true })
  }
  hum = () => {
    this.setState({ humVisible: true })
  }
  lux = () => {
    this.setState({ luxVisible: true })
  }

  render() {
    let temperatureMessage;
    let humidityMessage;
    let luxMessage;

    if (this.state.temperature >= 27) {
      temperatureMessage = "Too hot.";
    }
    else if (this.state.temperature < 27 && this.state.temperature > 22) {
      temperatureMessage = "That's ideal.";
    }
    else {
      temperatureMessage = "Cold inside!";
    }

    if (this.state.humidity >= 47) {
      humidityMessage = "Too wet air.";
    }
    else if (this.state.humidity < 47 && this.state.temperature > 43) {
      humidityMessage = "That's ideal.";
    }
    else {
      humidityMessage = "The air si too dry!";
    }

    if (this.state.lux <= 150) {
      luxMessage = "It's dark outside, switching lights on.";
    }
    else if (this.state.lux > 150 && this.state.lux <= 10000) {
      luxMessage = "That's ideal.";
    }
    else {
      luxMessage = "It's too bright!";
    }


    return (
      <React.Fragment>
        <View style={floorPlan.heading}>
          <Text style={floorPlan.h1}>Climate information</Text>
          <Text style={floorPlan.h2}>about Main hall</Text>
        </View>

        <View style={{marginTop: 40}}>
          <Overlay width='45%' height='auto' isVisible={this.state.tempVisible}
          onBackdropPress={() => this.setState({ tempVisible: false })}>
            <View style={overlayFloorStyles.wrapper}>
              <Icon style={styles.icon} name={Platform.OS === 'ios' ? 'ios-thermometer' : 'md-thermometer'} size={32} color="#F45937"/>
              <Text style={overlayFloorStyles.h1}>{this.state.temperature}°C</Text>
            </View>
          </Overlay>
          
          <Overlay width='45%' height='auto' isVisible={this.state.luxVisible}
          onBackdropPress={() => this.setState({ luxVisible: false })}>
            <View style={overlayFloorStyles.wrapper}>
              <Icon style={styles.icon} name={Platform.OS === 'ios' ? 'ios-water' : 'md-water'} size={32} color="#F45937"/>
              <Text style={overlayFloorStyles.h1}>{this.state.humidity}%</Text>
            </View>
          </Overlay>

          <Overlay width='45%' height='auto' isVisible={this.state.humVisible}
          onBackdropPress={() => this.setState({ humVisible: false })}>
            <View style={overlayFloorStyles.wrapper}>
              <Icon style={styles.icon} name={this.state.lux >= 150 ? (Platform.OS === 'ios' ? 'ios-sunny' : 'md-sunny') : (Platform.OS === 'ios' ? 'ios-moon' : 'md-moon')} size={32} color="#F45937"/>
              <Text style={overlayFloorStyles.h1}>{this.state.lux} lx</Text>
            </View>
          </Overlay>
        </View>

        <View>
          <View>
            <View style={styles.wrapperOfWrapperBox}>
              <View style={styles.iconsBoxes}>
                <View style={styles.iconWithText}>
                  <Icon style={styles.icon} name={Platform.OS === 'ios' ? 'ios-thermometer' : 'md-thermometer'} size={28} color="#F45937"/>
                </View>
                <View>            
                  <Text style={styles.valueOfSensors}>{temperatureMessage}</Text>
                </View>
              </View>

              <View style={styles.iconsBoxes}>
                <View style={styles.iconWithText}>
                  <Icon style={styles.icon} name={Platform.OS === 'ios' ? 'ios-water' : 'md-water'} size={28} color="#F45937"/>
                </View>
                <View>            
                  <Text style={styles.valueOfSensors}>{humidityMessage}</Text>
                </View>
              </View>

              <View style={styles.iconsBoxes}>
                <View style={styles.iconWithText}>
                  <Icon 
                    style={styles.icon} 
                    name={this.state.lux >= 150 ? (Platform.OS === 'ios' ? 'ios-sunny' : 'md-sunny') : (Platform.OS === 'ios' ? 'ios-moon' : 'md-moon')} 
                    size={28}
                    color="#F45937"
                  />
                </View>
                <View>            
                  <Text style={styles.valueOfSensors}>{luxMessage}</Text>
                </View>
              </View>

              <View style={{marginTop: 50}}>
                <View style={{width: 300, height: 200, backgroundColor: "#d9d9d9"}} />
                <View style={{position: "absolute", top: 30, left: -10, width: 10, height: 100, backgroundColor: "#d9d9d9"}} />
                <TouchableOpacity onPress={this.hum} style={{position: "absolute", top: 175, left: 50, width: 25, height: 25, borderRadius: 100, backgroundColor: "#F45937"}} />
                <TouchableOpacity onPress={this.dht} style={{position: "absolute", top: 175, left: 100, width: 25, height: 25, borderRadius: 100, backgroundColor: "#F45937"}} />
                <TouchableOpacity onPress={this.lux} style={{position: "absolute", top: 0, left: 230, width: 25, height: 25, borderRadius: 100, backgroundColor: "#F45937"}} />
              </View>
            </View>
          </View>
        </View>
      </React.Fragment>
    );
  }
}