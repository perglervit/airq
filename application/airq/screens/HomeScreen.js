import React, { Component } from 'react';
import { db } from '../firebase';
import {
  Text,
  View,
  ScrollView,
} from 'react-native';
import ValueBoxes from '../components/ValueBoxes';
import { headingStyle, styles } from '../css/styles'

let humidityRef = db.ref('/DHT11/Humidity').limitToLast(1);
let temperatureRef = db.ref('/DHT11/Temperature').limitToLast(1);
let luxRef = db.ref('/Photoresistor/Value').limitToLast(1);
let humidityRef2 = db.ref('/DHT11_2/Humidity').limitToLast(1);
let temperatureRef2 = db.ref('/DHT11_2/Temperature').limitToLast(1);
let luxRef2 = db.ref('/Photoresistor_2/Value').limitToLast(1);

export default class HomeScreen extends Component {
  state = {
    humidity1: [],
    temperature1: [],
    lux1: [],
    humidity2: [],
    temperature2: [],
    lux2: []
  }

  componentDidMount(){
    humidityRef.on('value' , (snap) => {
      let data = snap.val();
      let humidity1 = Object.values(data);
      this.setState({ humidity1 })
    })
    temperatureRef.on('value' , (snap) => {
      let data = snap.val();
      let temperature1 = Object.values(data);
      this.setState({ temperature1 })
    })
    luxRef.on('value' , (snap) => {
      let data = snap.val();
      let lux1 = Object.values(data);
      lux1 = Math.round(250 * lux1)
      this.setState({ lux1 })
    })
    humidityRef2.on('value' , (snap) => {
      let data = snap.val();
      let humidity2 = Object.values(data);
      this.setState({ humidity2 })
    })
    temperatureRef2.on('value' , (snap) => {
      let data = snap.val();
      let temperature2 = Object.values(data);
      this.setState({ temperature2 })
    })
    luxRef2.on('value' , (snap) => {
      let data = snap.val();
      let lux2 = Object.values(data);
      lux2 = Math.round(250 * lux2)
      this.setState({ lux2 })
    })
  }
  
  render() {
    return (
      <React.Fragment>
        <View style={headingStyle.heading}>
          <Text style={headingStyle.h1}>Hi,</Text>
          <Text style={headingStyle.h2}>Hackathon!</Text>
          <Text style={headingStyle.h3}>Climate in rooms:</Text>
        </View>
        <ScrollView>
          <ValueBoxes 
            roomName="Conference room"
            temperature={this.state.temperature1}
            humidity={this.state.humidity1}
            coAmount="x"
            pressure="x"
            sunIntensity={this.state.lux1}
          />
          <ValueBoxes 
            roomName="Main hall"
            temperature={this.state.temperature2}
            humidity={this.state.humidity2}
            coAmount="x"
            pressure="x"
            sunIntensity={this.state.lux2}
          />
        </ScrollView>
      </React.Fragment>
    )
  }
}

