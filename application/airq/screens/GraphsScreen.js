import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  Text,
  View,
  Button,
  TouchableOpacity,
} from 'react-native';

import { floorPlan, styles, headingStyle } from '../css/styles'

export default class GraphScreen extends Component {
  state = {
    humidity: [],
    temperature: [],
    lux: []
  }

  
  invert() {
    
  }

  render() {
    return(
      <React.Fragment>
        <View style={floorPlan.heading}>
          <Text style={floorPlan.h1}>Settings</Text>
        </View>
        <View>
          <Button title="Toggle dark mode" onPress="invert" type="outline"/>
        </View>
      </React.Fragment>
    );
  }
}